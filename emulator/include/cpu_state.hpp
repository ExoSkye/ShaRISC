
#ifndef SHARISC_CPU_STATE_HPP
#define SHARISC_CPU_STATE_HPP

#include "int_types.h"

struct CpuState {
    u32 mutable_registers[15] = { 0 };
    u32 _r0 = 0;
    u32* r0 = &_r0;
    u32* r1 = &mutable_registers[0];
    u32* r2 = &mutable_registers[1];
    u32* r3 = &mutable_registers[2];
    u32* r4 = &mutable_registers[3];
    u32* r5 = &mutable_registers[4];
    u32* r6 = &mutable_registers[5];
    u32* r7 = &mutable_registers[6];
    u32* r8 = &mutable_registers[7];
    u32* r9 = &mutable_registers[8];
    u32* r10 = &mutable_registers[9];
    u32* r11 = &mutable_registers[10];
    u32* r12 = &mutable_registers[11];
    u32* r13 = &mutable_registers[12];
    u32* r14 = &mutable_registers[13];
    u32* r15 = &mutable_registers[14];

    u32* ihtr = &mutable_registers[10];
    u32* sp = &mutable_registers[11];
    u32* cmpr = &mutable_registers[12];
    u32* lpcr = &mutable_registers[13];
    u32* pc = &mutable_registers[14];

    bool halted = false;
    u8* memory = nullptr;

    u16 interrupt_mask = 0x0000;
    s8 current_interrupt = -1;

    bool branched = false;
};

#endif //SHARISC_CPU_STATE_HPP
