# ShaRISC

This is an extreme RISC ISA, which is defined by the `docs.md` file in this repository.

In this repository, you'll find an emulator, an assembler, and the documentation for the ISA.

