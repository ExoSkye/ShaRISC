.at 0
.skip 0
.byte 1
.short 2
.align 4
a:
.word &a
.string "aa"
NOP
BR 1
BR r0
BR r0, 1
BR *r0
BR *r0, 1
CBR 1
CBR r0
CBR r0, 1
CBR *r0
CBR *r0, 1
LDR r0, 1
LDR *r0, 1
LDR *r0, r1, 1
STR r0, r1
STR *r0, r1
STR *r0, r1, 1
OFF r0, 1
OFF r0, r1
ABS r0
CMP r0, 1
CMP r0, r1
OR r0, 1
OR r0, r1
AND r0, 1
AND r0, r1
NOT r0
RO r0, 1
RO r0, r1
RIN 1
SIM 1
HLT
RST