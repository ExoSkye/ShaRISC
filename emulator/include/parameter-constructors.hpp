
#ifndef SHARISC_PARAMETER_CONSTRUCTORS_HPP
#define SHARISC_PARAMETER_CONSTRUCTORS_HPP

#include "instructions/enums.hpp"
#include "int_types.h"

u32 construct_n() {
    return 0;
}

u32 construct_i(u16 i) {
    return (i & 0xFFFF) << 8;
}

u32 construct_r(u32 r) {
    return (r & 0b1111) << 8;
}

u32 construct_rr(u32 r1, u32 r2) {
    return (r1 & 0b1111) << 8 | (r2 & 0x1111) << 12;
}

u32 construct_ir(u32 i, u32 r) {
    return (r & 0b1111) << 8 | (i & 0xFFFF) << 12;
}

u32 construct_iir(u32 i1, u32 i2, u32 r) {
    return (r & 0b1111) << 8 | (i1 & 0xF) << 12 | (i2 & 0xF) << 20;
}

u32 construct_irr(u32 i, u32 r1, u32 r2) {
    return (r1 & 0b1111) << 8 | (r2 & 0x1111) << 12 | (i & 0xFF) << 16;
}


u32 construct_rrr(u32 r1, u32 r2, u32 r3) {
    return (r1 & 0b1111) << 8 | (r2 & 0b1111) << 12 | (r3 & 0b1111) << 16;
}

u32 construct_iii(u32 i1, u32 i2, u32 i3) {
    return (i1 & 0b1111) << 8 | (i2 & 0xFF) << 16 | (i3 & 0xFF) << 24;
}

u32 construct_m(u32 m) {
    return construct_r(m);
}

u32 construct_mr(u32 m, u32 r) {
    return construct_rr(m, r);
}

u32 construct_mi(u32 m, u32 i) {
    return construct_ir(i, m);
}

u32 construct_mri(u32 m, u32 r, u32 i) {
    return construct_irr(m, r, i);
}

#endif //SHARISC_PARAMETER_CONSTRUCTORS_HPP
