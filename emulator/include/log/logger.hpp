
#ifndef SHARISC_LOGGER_HPP
#define SHARISC_LOGGER_HPP

#ifdef LOGGING

#include <instructions/instructions.hpp>
#include <instructions/structs.hpp>
#include <int_types.h>

constexpr char register_name_map[16][5] = {
        "r0",
        "r1",
        "r2",
        "r3",
        "r4",
        "r5",
        "r6",
        "r7",
        "r8",
        "r9",
        "r10",
        "ihtr",
        "sp",
        "cmpr",
        "lpcr",
        "pc",
};

inline void log_instruction(instruction_t instruction, CpuState *state) {
    printf("%08x @ %08x:\t%s\t", htobe32(*(u32 *) &instruction), get_register(15, state), opcode_map[instruction.opcode]);

    u32 params = instruction.parameters;

    switch (instruction.param_mode) {
        case 1: {
            param_mode_I p = *(param_mode_I *) &params;
            printf("%i", (s16) (p.i));
        }
            break;
        case 2: {
            param_mode_R p = *(param_mode_R *) &params;
            printf("%s { %08x }", register_name_map[p.r], get_register(p.r, state));
        }
            break;
        case 3: {
            param_mode_RR p = *(param_mode_RR *) &params;
            printf("%s { %08x }, %s { %08x }", register_name_map[p.r1], get_register(p.r1, state),
                   register_name_map[p.r2], get_register(p.r2, state));
        }
            break;
        case 4: {
            param_mode_RI p = *(param_mode_RI *) &params;
            printf("%s { %08x }, %i", register_name_map[p.r], get_register(p.r, state), (s16) (p.i));
        }
            break;
        case 5: {
            param_mode_RII p = *(param_mode_RII *) &params;
            printf("%s { %08x }, %i, %i", register_name_map[p.r], get_register(p.r, state), (s8) (p.i1), (s8) (p.i2));
        }
            break;
        case 6: {
            param_mode_RRI p = *(param_mode_RRI *) &params;
            printf("%s { %08x }, %s { %08x }, %i", register_name_map[p.r1], get_register(p.r1, state),
                   register_name_map[p.r2], get_register(p.r2, state), (s16) (p.i));

        }
            break;
        case 7: {
            param_mode_RRR p = *(param_mode_RRR *) &params;
            printf("%s { %08x }, %s { %08x }, %s { %08x }", register_name_map[p.r1], get_register(p.r1, state),
                   register_name_map[p.r2], get_register(p.r2, state), register_name_map[p.r3],
                   get_register(p.r3, state));
        }
            break;
        case 8: {
            param_mode_III p = *(param_mode_III *) &params;
            printf("%i, %i, %i", (s8) (p.i1), (s8) (p.i2), (s8) (p.i3));
        }
            break;
        case 9: {
            param_mode_M p = *(param_mode_M *) &params;
            printf("*%s { %08x -> %08x }", register_name_map[p.m], get_register(p.m, state),
                   deref_get_memory(p.m, state));
        }
            break;
        case 10: {
            param_mode_MR p = *(param_mode_MR *) &params;
            printf("*%s { %08x -> %08x }, %s { %08x }", register_name_map[p.m], get_register(p.m, state),
                   deref_get_memory(p.m, state), register_name_map[p.r], get_register(p.r, state));
        }
            break;
        case 11: {
            param_mode_MI p = *(param_mode_MI *) &params;
            printf("*%s { %08x -> %08x }, %i", register_name_map[p.m], get_register(p.m, state),
                   deref_get_memory(p.m, state), (s16) (p.i));
        }
            break;
        case 12: {
            param_mode_MRI p = *(param_mode_MRI *) &params;
            printf("*%s { %08x -> %08x }, %s { %08x }, %i", register_name_map[p.m], get_register(p.m, state),
                   deref_get_memory(p.m, state), register_name_map[p.r], get_register(p.r, state), (s16) (p.i));
        }
            break;

        default:
            break;
    }

    printf("\n");
}

#else

void log_instruction(instruction_t, CpuState*) {}

#endif

#endif //SHARISC_LOGGER_HPP
