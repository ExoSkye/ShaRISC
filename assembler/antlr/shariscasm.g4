grammar shariscasm;

WS: [ \t] -> skip;

NEWLINE             : [\r\n]+;
COMMENT_START       : ';';
COMMENT             : COMMENT_START  ~[\r\n]* -> skip;

NOP                 : 'NOP';
BR                  : 'BR';
CBR                 : 'CBR';
LDR                 : 'LDR';
STR                 : 'STR';
OFF                 : 'OFF';
ABS                 : 'ABS';
CMP                 : 'CMP';
OR                  : 'OR';
AND                 : 'AND';
NOT                 : 'NOT';
RO                  : 'RO';
RIN                 : 'RIN';
SIM                 : 'SIM';
HLT                 : 'HLT';
RST                 : 'RST';

fragment SIGN       : '-' | '+';
fragment DIGIT      : [0-9];
fragment HEXDIGIT   : [0-9a-fA-F];
fragment HEXPREFIX  : '0x';

fragment DEC        : SIGN? DIGIT? DIGIT? DIGIT? DIGIT? DIGIT? DIGIT? DIGIT? DIGIT? DIGIT? DIGIT;
fragment HEX        : SIGN? HEXPREFIX HEXDIGIT? HEXDIGIT? HEXDIGIT? HEXDIGIT? HEXDIGIT? HEXDIGIT? HEXDIGIT? HEXDIGIT;

INT                 : DEC | HEX;

REGISTER_0          : 'r0';
REGISTER_1          : 'r1';
REGISTER_2          : 'r2';
REGISTER_3          : 'r3';
REGISTER_4          : 'r4';
REGISTER_5          : 'r5';
REGISTER_6          : 'r6';
REGISTER_7          : 'r7';
REGISTER_8          : 'r8';
REGISTER_9          : 'r9';
REGISTER_10         : 'r10';
REGISTER_11         : 'r11';
REGISTER_12         : 'r12';
REGISTER_13         : 'r13';
REGISTER_14         : 'r14';
REGISTER_15         : 'r15';

REGISTER_IHTR       : 'ihtr';
REGISTER_SP         : 'sp';
REGISTER_CMPR       : 'cmpr';
REGISTER_LPCR       : 'lpcr';
REGISTER_PC         : 'pc';

COMMA               : ',';
DEREF               : '*';
COLON               : ':';
QUOTE               : '"';
DOT                 : '.';
BACKSLASH           : '\\';
AMPERSAND           : '&';

DIRECTIVE_BYTE      : DOT 'byte';
DIRECTIVE_WORD      : DOT 'word';
DIRECTIVE_SHORT     : DOT 'short';
DIRECTIVE_AT        : DOT 'at';
DIRECTIVE_SKIP      : DOT 'skip';
DIRECTIVE_STRING    : DOT 'string';
DIRECTIVE_ALIGN     : DOT 'align';

STRING              : QUOTE ~[\r\n"]* QUOTE;
IDENTIFIER          : [a-zA-Z] [a-zA-Z0-9]*;

r0                  : REGISTER_0;
r1                  : REGISTER_1;
r2                  : REGISTER_2;
r3                  : REGISTER_3;
r4                  : REGISTER_4;
r5                  : REGISTER_5;
r6                  : REGISTER_6;
r7                  : REGISTER_7;
r8                  : REGISTER_8;
r9                  : REGISTER_9;
r10                 : REGISTER_10;
r11                 : REGISTER_11 | REGISTER_IHTR;
r12                 : REGISTER_12 | REGISTER_SP;
r13                 : REGISTER_13 | REGISTER_CMPR;
r14                 : REGISTER_14 | REGISTER_LPCR;
r15                 : REGISTER_15 | REGISTER_PC;

register            : r0 | r1 | r2 | r3 | r4 | r5 | r6 | r7 | r8 | r9 | r10 | r11 | r12 | r13 | r14 | r15;

int                 : INT;

nop                 : NOP;
br                  : BR;
cbr                 : CBR;
ldr                 : LDR;
str                 : STR;
off                 : OFF;
abs                 : ABS;
cmp                 : CMP;
or                  : OR;
and                 : AND;
not                 : NOT;
ro                  : RO;
rin                 : RIN;
sim                 : SIM;
hlt                 : HLT;
rst                 : RST;

label_identifier    : IDENTIFIER;
label               : label_identifier COLON;

param_mode_n        :;
param_mode_i        : int;
param_mode_l        : label_identifier; // Label version of I
param_mode_r        : register;
param_mode_rr       : register COMMA register;
param_mode_ri       : register COMMA int;

// ---- Currently unused
//param_mode_rii      : register COMMA int COMMA int;
//param_mode_rri      : register COMMA register COMMA int;
//param_mode_rrl      : register COMMA register COMMA label_identifier;
//param_mode_rrr      : register COMMA register COMMA register;
//param_mode_iii      : int COMMA int COMMA int;
// ----

param_mode_m        : DEREF register;
param_mode_mr       : DEREF register COMMA register;
param_mode_mi       : DEREF register COMMA int;
param_mode_mri      : DEREF register COMMA register COMMA int;
param_mode_lr       : label_identifier COMMA register; // Label version of MRI

qual_nop            : nop (param_mode_n);
qual_br             : br (param_mode_i | param_mode_l | param_mode_r | param_mode_ri | param_mode_m | param_mode_mi);
qual_cbr            : cbr (param_mode_i | param_mode_l | param_mode_r | param_mode_ri | param_mode_m | param_mode_mi);
qual_ldr            : ldr (param_mode_ri | param_mode_mi | param_mode_mri | param_mode_lr);
qual_str            : str (param_mode_rr | param_mode_mr | param_mode_mri | param_mode_lr);
qual_off            : off (param_mode_ri | param_mode_rr);
qual_abs            : abs (param_mode_r);
qual_cmp            : cmp (param_mode_ri | param_mode_rr);
qual_or             : or (param_mode_ri | param_mode_rr);
qual_and            : and (param_mode_ri | param_mode_rr);
qual_not            : not (param_mode_r);
qual_ro             : ro (param_mode_ri | param_mode_rr);
qual_rin            : rin (param_mode_i);
qual_sim            : sim (param_mode_i);
qual_hlt            : hlt (param_mode_n);
qual_rst            : rst (param_mode_n);

instruction         : qual_nop
                    | qual_br
                    | qual_cbr
                    | qual_ldr
                    | qual_str
                    | qual_off
                    | qual_abs
                    | qual_cmp
                    | qual_or
                    | qual_and
                    | qual_not
                    | qual_ro
                    | qual_rin
                    | qual_sim
                    | qual_hlt
                    | qual_rst;

string              : STRING;

directive_byte      : DIRECTIVE_BYTE int;
directive_short     : DIRECTIVE_SHORT int;
directive_word_label: DIRECTIVE_WORD AMPERSAND label_identifier;
directive_word      : DIRECTIVE_WORD int;
directive_at        : DIRECTIVE_AT int;
directive_skip      : DIRECTIVE_SKIP int;
directive_string    : DIRECTIVE_STRING string;
directive_align     : DIRECTIVE_ALIGN int;

directive           : directive_byte
                    | directive_short
                    | directive_word
                    | directive_word_label
                    | directive_at
                    | directive_skip
                    | directive_string
                    | directive_align;

instruction_list    : ((instruction | label | directive)? NEWLINE)* (instruction | label | directive)? EOF;