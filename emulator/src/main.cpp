#include <cstdio>
#include <instructions/instructions.hpp>
#include <instructions/general.hpp>
#include "instructions/structs.hpp"
#include <log/logger.hpp>

#include <tracy/Tracy.hpp>
#include <string>

int main(int argc, char** argv) {
    ZoneScopedN("Main");

    size_t mem_size = 0x7FFFF;

    u8* memory;
    {
        ZoneScopedN("Allocate memory");
        memory = (u8*)(calloc(1, mem_size));
    }

    if (argc < 2) {
        return 1;
    }

    {
        ZoneScopedN("Read in program file");
        FILE *f = fopen(argv[1], "r");

        fseek(f, 0, SEEK_END);
        size_t len = ftell(f);
        fseek(f, 0, SEEK_SET);

        fread(memory, len, 1, f);

        fclose(f);
    }

    {
        ZoneScopedN("Setup ISA");
        setup_isa();
    }

    {
        ZoneScopedN("Setup logging helpers");
#ifdef LOGGING
        setup_opcode_map();
        setup_param_mode_map();
#endif

    }

    bool debug = false;

    if (argc == 3) {
        if (std::string(argv[2]) == "-d") {
            debug = true;
        }
    }

    CpuState state;
    state.memory = memory;

    while (!state.halted) {
        ZoneScopedN("Instruction Execution");

        u32 inst_raw = get_memory(get_register(15, &state), &state);

        instruction_t inst = *(instruction_t*)&inst_raw;

        log_instruction(inst, &state);

        isa[inst.id](&state, inst.parameters);

        if (debug)
            getchar();

        if (!state.branched) {
            set_register(15, get_register(15, &state) + 4, &state);
        } else {
            state.branched = false;
        }

        if (state.current_interrupt != -1 && ((state.interrupt_mask >> state.current_interrupt) & 0b1) == 1) {
            if (get_register(11, &state) != 0) {
                set_register(14, get_register(15, &state), &state);
                set_register(15, get_register(11, &state) + (state.current_interrupt * 4 * 4), &state);
            } else {
                state.halted = true;
            }
        }
    }

    free(memory);

    return 0;
}
