#ifndef SHARISC_GENERAL_HPP
#define SHARISC_GENERAL_HPP

#include "../cpu_state.hpp"
#include "int_types.h"

inline u32 get_register(const u8 i, const CpuState* state) {
    if (i == 0) {
        return 0;
    } else {
        return be32toh(state->mutable_registers[i - 1]);
    }
}

inline u32 set_register(const u8 i, const u32 val, CpuState* state) {
    assert(i != 0);

    state->mutable_registers[i - 1] = htobe32(val);
    return val;
}

inline u32 get_memory(const u32 addr, const CpuState* state) {
    return be32toh(*(u32*)&state->memory[addr]);
}

inline u32 set_memory(const u32 addr, const u32 val, const CpuState* state) {
    *(u32*)&state->memory[addr] = htobe32(val);
    return val;
}

inline u32 deref_get_memory(const u8 reg, const CpuState* state) {
    return get_memory(get_register(reg, state), state);
}

inline u32 deref_set_memory(const u8 reg, const u32 val, const CpuState* state) {
    return set_memory(get_register(reg, state), val, state);
}

inline void branch_general(const u32 new_address, CpuState* state) {
    set_register(14, get_register(15, state) + 4, state);
    set_register(15, new_address, state);
    (state->branched) = true;
}

inline void conditional_branch_general(const u32 new_address, CpuState* state) {
    if ((*state->cmpr) & 0b1) {
        branch_general(new_address, state);
    }
}

inline void offset_general(const s32 input, const u32 register_num, CpuState* state) {
    set_register(register_num, get_register(register_num, state) + input, state);
}

#define SET_BIT(src, bit, dest) dest |= (((u8)(src) & 0b1) << bit)

inline void compare_general(s32 a, s32 b, CpuState* state) {
    u32 cmpr = get_register(13, state);
    SET_BIT(a == b, 0, cmpr);
    SET_BIT(a > b, 1, cmpr);
    SET_BIT(a < b, 2, cmpr);
    SET_BIT(abs(a) == abs(b), 3, cmpr);
    set_register(13, cmpr, state);
}

inline void or_general(const u32 input, const u32 register_num, CpuState* state) {
    set_register(register_num, get_register(register_num, state) | input, state);
}

inline void and_general(const u32 input, const u32 register_num, CpuState* state) {
    set_register(register_num, get_register(register_num, state) & input, state);
}

inline void ro_general(s32 input, const u32 register_num, CpuState* state) {
    // stolen from SO
    input &= 31;

    set_register(register_num, (input << get_register(register_num, state)) | (input >> ((-get_register(register_num, state))&31 )), state);
}

#endif //SHARISC_GENERAL_HPP
