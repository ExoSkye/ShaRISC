
#ifndef SHARISC_ENUMS_HPP
#define SHARISC_ENUMS_HPP

enum Instructions {
    NOP,
    BR,
    CBR,
    LDR,
    STR,
    OFF,
    ABS,
    CMP,
    OR,
    AND,
    NOT,
    RO,
    RIN,
    SIM,
    HLT,
    RST
};

enum ParameterTypes {
    N,
    I,
    R,
    RR,
    RI,
    RII,
    RRI,
    RRR,
    III,
    M,
    MR,
    MI,
    MRI
};

#endif //SHARISC_ENUMS_HPP
