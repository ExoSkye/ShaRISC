#ifndef SHARISC_INSTRUCTIONS_HPP
#define SHARISC_INSTRUCTIONS_HPP

#include <cstring>
#include "defs.hpp"

void invalid_instruction(CpuState* state, u32) {
    state->current_interrupt = 0;
}

void (*isa[0x100])(CpuState*, u32);

#define NOT_INITIAL_DEFINE
#define DONT_INCLUDE_GENERAL
#define OP_PT(o, p) (o << 4 | p)
#undef IDEF_FN
#define IDEF_FN(o, p, ...) isa[OP_PT(o,p)] = &COMBINE2(o,p);

void setup_isa() {
    for (size_t i = 0; i < 0x100; i++) {
        isa[i] = &invalid_instruction;
    }

#include "defs.hpp"
}

#undef IDEF_FN
#define IDEF_FN(o, p, ...) strcpy(opcode_map[o], #o);

char opcode_map[16][4];

void setup_opcode_map() {
#include "defs.hpp"
}

#undef IDEF_FN
#define IDEF_FN(o, p, ...) strcpy(param_mode_map[p], #p);

char param_mode_map[16][4];

void setup_param_mode_map() {
#include "defs.hpp"
}

#endif //SHARISC_INSTRUCTIONS_HPP
