#ifndef SHARISC_STRUCTS_HPP
#define SHARISC_STRUCTS_HPP

#include "packed.h"
#include "int_types.h"

typedef packed_struct instruction_t {
    union {
        struct {
            u8 param_mode: 4;
            u8 opcode: 4;
        };
        u8 id : 8;
    };
    u32 parameters : 24;
} instruction;

typedef packed_struct param_mode_N {} param_mode_N;

typedef packed_struct param_mode_I {
    u16 i : 16;
} param_mode_I;

typedef packed_struct param_mode_R {
    u8 r : 4;
} param_mode_R;

typedef packed_struct param_mode_RR {
    u8 r1 : 4;
    u8 r2 : 4;
} param_mode_RR;

typedef packed_struct param_mode_RI {
    u8 r : 4;
    u16 i : 16;
} param_mode_RI;

typedef packed_struct param_mode_RII {
    u8 r : 4;
    u8 i1 : 8;
    u8 i2 : 8;
} param_mode_RII;

typedef packed_struct param_mode_RRI {
    u8 r1 : 4;
    u8 r2 : 4;
    u8 i : 8;
} param_mode_RRI;

typedef packed_struct param_mode_RRR {
    u8 r1 : 4;
    u8 r2 : 4;
    u8 r3 : 4;
} param_mode_RRR;

typedef packed_struct param_mode_III {
    u8 i1 : 8;
    u8 i2 : 8;
    u8 i3 : 8;
} param_mode_III;

typedef packed_struct param_mode_M {
    u8 m : 4;
} param_mode_M;

typedef packed_struct param_mode_MR {
    u8 m : 4;
    u8 r : 4;
} param_mode_MR;

typedef packed_struct param_mode_MI {
    u8 m : 4;
    u16 i : 16;
} param_mode_MI;

typedef packed_struct param_mode_MRI {
    u8 m : 4;
    u8 r : 4;
    u16 i : 16;
} param_mode_MRI;

typedef struct meta_param_mode {
    union {
        param_mode_N n;
        param_mode_I i;
        param_mode_R r;
        param_mode_RR rr;
        param_mode_RI ri;
        param_mode_RII rii;
        param_mode_RRI rri;
        param_mode_RRR rrr;
        param_mode_III iii;
        param_mode_M m;
        param_mode_MR mr;
        param_mode_MI mi;
        param_mode_MRI mri;
        u32 all : 24;
    };
} meta_param_mode;

#endif //SHARISC_STRUCTS_HPP
