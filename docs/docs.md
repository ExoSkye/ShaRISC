# ShaRISC

Version: `8.2`

## Endianness

This is a big-endian ISA, instructions are stored in Big Endian format, memory reads/writes are also big endian.

## Instruction Layout

Instructions have to be aligned on a 4 byte boundary, and are 32 bits long

One character = one bit

```
/--\/--\/----------------------\
A   B   C
```

where:  
A = Opcode (see [Instructions](#instructions))  
B = Addressing mode (see [Parameter Modes](#instruction-parameter-modes))  
C = Parameters (based on the value of B)

## Instruction Parameter Modes

Note: memory addresses in the below chart refer to a register that contains a memory address

### Key

| Type            | Symbol | Length (bits) |
|-----------------|--------|---------------|
| Register        | `R`    | 4             |
| Memory Address  | `M`    | 4             | 
| Immediate Value | `I`    | 8/16 bits     |          
| Ignored         | `0`    | Any           |

Example:  
`MMMM1RRR2RRRRIIIIIIII0000` means that:  
1. The first 4 bits contain the register number of a register that contains a memory address, and will be interpreted as such  
2. The second 4 bits contain the first register number  
3. The third 4 bits also contain a second register number  
4. The next 8 bits are an immediate value   
5. The last 4 bits are ignored, but should be set to 0  

| Value | Code  | Meaning                                         | Layout                     |
|-------|-------|-------------------------------------------------|----------------------------|
| 0000  | `N`   | No parameters                                   | `000000000000000000000000` |
| 0001  | `I`   | Immediate                                       | `IIIIIIIIIIIIIIII00000000` |
| 0010  | `R`   | Register                                        | `RRRR00000000000000000000` |
| 0011  | `RR`  | Two registers                                   | `1RRR2RRR0000000000000000` |
| 0100  | `RI`  | Immediate and one register                      | `RRRRIIIIIIIIIIIIIIII0000` |
| 0101  | `RII` | Two immediates, one register                    | `RRRR1IIIIIII2IIIIIII0000` |
| 0110  | `RRI` | One immediate, two registers                    | `1RRR2RRRIIIIIIIIIIIIIIII` |
| 0111  | `RRR` | Three registers                                 | `1RRR2RRR3RRR000000000000` |
| 1000  | `III` | Three immediates                                | `1IIIIIII2IIIIIII3IIIIIII` |
| 1001  | `M`   | One memory address                              | `MMMM00000000000000000000` |
| 1010  | `MR`  | One memory address, one register                | `MMMMRRRR0000000000000000` |
| 1011  | `MI`  | One memory address, one immediate               | `MMMMIIIIIIIIIIIIIIII0000` |
| 1100  | `MRI` | One memory address, one register, one immediate | `MMMMRRRRIIIIIIIIIIIIIIII` |

\pagebreak

## Registers

| Register name | Alternate Name | Usage                                         | Code |
|---------------|----------------|-----------------------------------------------|------|
| r0            |                | Always equal to 0, writes are ignored         | 0000 |
| r1            |                | General purpose                               | 0001 |
| r2            |                | General purpose                               | 0010 |
| r3            |                | General purpose                               | 0011 |
| r4            |                | General purpose                               | 0100 |
| r5            |                | General purpose                               | 0101 |
| r6            |                | General purpose                               | 0110 |
| r7            |                | General purpose                               | 0111 |
| r8            |                | General purpose                               | 1000 |
| r9            |                | General purpose                               | 1001 |
| r10           |                | General purpose                               | 1010 |
| ihtr          | r11            | Memory Address of the Interrupt Handler Table | 1011 |
| sp            | r12            | Stack Pointer                                 | 1100 |
| cmpr          | r13            | Comparison Result Register                    | 1101 |
| lpcr          | r14            | Last Program Counter Register                 | 1110 |
| pc            | r15            | Program Counter                               | 1111 |

At boot, every register is set to 0, other than `pc` which is set to wherever the CPU is set to boot from.

### `cmpr` Notes

| Bit number | Meaning                                             | Code equivalent  |
|------------|-----------------------------------------------------|------------------|
| 0          | A equals B                                          | A == B           |
| 1          | A is bigger than B                                  | A > B            |
| 2          | A is smaller than B                                 | A < B            |
| 3          | Absolute value of A is equal to absolute value of B | abs(A) == abs(B) |
| 4+         | Reserved                                            | N/A              |

\pagebreak

## Instructions
### No-op

Mnemonic = `NOP`  
Opcode = `0000`  
Parameter Modes = `N`

No operation.

### Branch

Mnemonic = `BR`  
Opcode = `0001`  
Parameter Modes = `I`/`R`/`RI`/`M`/`MI`

Saves the current `pc` in `lpcr` and then sets `pc` to the value specified in the parameters.

`I`: The immediate value is interpreted as two's complement and added to the current `pc` value after being multiplied by 4 to make the value a jump in instructions, not bytes.

`R`: The value in the register in full is taken as the new `pc` value.

`RI`: The value in the register is offset by the immediate value (which is multiplied by 4 to make it an offset in instructions, not bytes and taken as two's complement) and the result is loaded into `pc`.

`M`: The value contained at the memory address provided is used as the new `pc`.

`MI`: The value contained at the memory address provided is offset by the immediate value (which is multiplied by 4 to make it an offset in instructions, not bytes and taken as two's complement) and the result is loaded into `pc`.

### Conditional Branch

Mnemonic = `CBR`  
Opcode = `0010`  
Parameter Modes = `I`/`R`/`RI`/`M`/`MI`

Exactly the same as [Branch](#branch), but the lowest bit of `cmpr` has to be set to 1.

### Load to register

Mnemonic = `LDR`  
Opcode = `0011`  
Parameter Modes = `RI`/`MR`/`MRI`

Load a value given in the parameter into the register specified.

`RI`: Load the immediate value into the register.

`MR`: Load the value in the memory address specified into the register.

`MRI`: Load the value in the memory address, with the offset specified by the immediate value into the register.

### Store from register

Mnemonic = `STR`  
Opcode = `0100`  
Parameter Modes = `RR`/`MR`/`MRI`

Stores a value from a register into a destination specified by the parameters.

`RR`: Load the value in the first register specified into the second register.

`MR`: Load the value in the register specified into the memory address. 

`MRI`: Load the value in the register into the memory address, which is offset by the immediate value.

### Offset

Mnemonic = `OFF`  
Opcode = `0101`  
Parameter Modes = `RI`/`RR`

Offsets the value in the destination register by the value specified in the source.

**Parameter Interpretation:**

`RI`: The immediate value is used as the source.

`RR`: The first register is used as the source.

### Absolute value

Mnemonic = `ABS`  
Opcode = `0110`  
Parameter Modes = `R`

Compute the absolute value of the register given, and store that back to the register.

### Compare

Mnemonic = `CMP`  
Opcode = `0111`  
Parameter Modes = `RI`/`RR`

Performs a comparison between the two values given, and stores the results in `cmpr`.  

`RI`: The immediate value is always `A`, whereas the register is `B`

`RR`: The first register is `A`, the second register is `B`

### Bitwise OR

Mnemonic = `OR`  
Opcode = `1000`  
Parameter Modes = `RI`/`RR`

Compute the bitwise OR of the input value (first register, or immediate value) and the second register, store result into the second register.

### Bitwise AND

Mnemonic = `AND`  
Opcode = `1001`  
Parameter Modes = `RI`/`RR`

Compute the bitwise AND of the input value (first register, or immediate value) and the second register, store result into the second register.

### Bitwise NOT

Mnemonic = `NOT`  
Opcode = `1010`  
Parameter Modes = `R`

Compute the bitwise NOT of the register given, store back in the register.

### Bitwise Rotate

Mnemonic = `RO`  
Opcode = `1011`  
Parameter Modes = `RI`/`RR`

Rotate the second register by the (two's complement encoded) input parameter (immediate value or the first register), store in the second register.
Positive values mean a rotation in the left direction.

### Interrupt Raise

Mnemonic = `RIN`  
Opcode = `1100`  
Parameter Modes = `I`

Raises the interrupt *x*, where *x* is the immediate value given.
Only the lower 4 bits are considered since there are only 16 interrupts.

### Set Interrupt Mask

Mnemonic = `SIM`  
Opcode = `1101`  
Parameter Modes = `I`  

Sets the interrupt mask, only the bottom 16 bits are considered, and each bit represents whether that interrupt will be caught (`1` = it will be caught, `0` = it will not be caught).

### Halt

Mnemonic = `HLT`  
Opcode = `1110`  
Parameter Modes = `N`

Halts the CPU, and waits for an interrupt.

### Reset

Mnemonic = `RST`  
Opcode = `1111`  
Parameter Modes = `N`

Resets the CPU.

\pagebreak

## Interrupt Handling

### Interrupt Flow

An interrupt triggers a jump to the corresponding offset in the interrupt handler table (where the base of the table is specified in `ihtr`) - See [Interrupt Handler Table](#interrupt-handler-table).
This happens after every instruction executed, when the interrupt isn't masked.

### Interrupt Meanings

| Number | Binary Value | Name                | Description                                                                |
|--------|--------------|---------------------|----------------------------------------------------------------------------|
| 0      | 0000         | Invalid Instruction | The instruction the CPU tried to execute was invalid                       |
| 1-7    | 0001-0111    | Reserved            | These interrupts are reserved for a future version, and should not be used |
| 8-15   | 1000-1111    | User                | These interrupts will always be available to a user                        |

### Interrupt Handler Table

There are 16 interrupts, so there are 16 entries in the interrupt handler table, where each are 4 instructions long. 
If `lpcr` is modified by a `BR` or a `CBR` instruction, it must be saved, and every interrupt handler must end in an instruction that returns,
either via loading the saved `lpcr` value into `pc` directly, or otherwise.  
It must not be placed at `0x0`, and has to be 4 byte aligned.

\pagebreak

## Examples

### Reset if value in `r1` is bigger than the value in `r2`

```
CMP r1,   r2        ; Compare r1 and r2
NOT cmpr            ; Not cmpr, so that bit 1 is now whether A is smaller than or equal to B
RO  cmpr, -1        ; Rotate by -1 so that bit 1 is selected as the used conditional
CBR 2               ; Skip the next instruction if r1 is smaller than or equal to r2
RST                 ; Reset
                    ; Rest of the code goes here
```

### Load a value relative to `pc` into `r1` and reset if `abs(r1) == abs(r2)` but `r1 != r2`

```
.word 0xF1F2F3F4    ; Define a 32 bit value at this address
LDR  *pc,  r1, -4   ; Load the 32 bit value into r1
CMP  r1,   r2       ; Compare r1 and r2
STR  cmpr, r3       ; Load the result into r3
RO   cmpr, -3       ; Rotate the result by -3
STR  cmpr, r4       ; Load the new result into r4
AND  r3,    1       ; And r3 with 1 to get whether they were equal
AND  r4,    1       ; And r4 with 1 to get whether their absolute values were equal
CMP  r4,   r3       ; Compare r4 and r3
RO   cmpr, -1       ; Rotate the result by -1 to select A > B
CBR  2              ; If it's set, jump to the `HLT` instruction
BR   2              ; If not, skip the instruction
RST                 ; Reset
                    ; Rest of the code goes here
```
