BR start
a:
.word 0xF1F2F3F4
start:
LDR a, r1 ; Load the 32 bit value into r1
CMP r1, r2 ; Compare r1 and r2
STR cmpr, r3 ; Load the result into r3
RO cmpr, -3 ; Rotate the result by -3
STR cmpr, r4 ; Load the new result into r4
AND r3, 1 ; And r3 with 1 to get whether they were equal
AND r4, 1 ; And r4 with 1 to get whether their absolute values were equal
CMP r3, r4 ; Compare r4 and r3
RO cmpr, -1 ; Rotate the result by -1 to select A > B
CBR reset ; If it's set, jump to the `HLT` instruction
BR skip ; If not, skip the instruction
reset:
RST ; Reset
skip:
; Rest of the code goes here
HLT ; Halt instead :3
.string "screw you"