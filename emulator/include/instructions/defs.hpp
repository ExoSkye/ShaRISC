#include "instructions/enums.hpp"

#define COMBINE(o, p) o##p
#define COMBINE2(o, p) COMBINE(o, p)

#ifndef NOT_INITIAL_DEFINE

#include "../cpu_state.hpp"
#include "instructions/structs.hpp"
#include <cassert>
#include <cmath>
#include "tracy/Tracy.hpp"

#define IDEF_FN(_o, _p, ...) void COMBINE2(_o,_p)(CpuState* state, u32 params) { ZoneScopedN(#_o " - " #_p); param_mode_##_p p = *(param_mode_##_p*)&params; __VA_ARGS__ }
#endif

#ifndef DONT_INCLUDE_GENERAL

#include "general.hpp"

#endif

// General defs

IDEF_FN(NOP, N, {})

IDEF_FN(BR, I, {
    branch_general(get_register(15, state) + (((s32)(s16)p.i) * 4), state);
})

IDEF_FN(BR, R, {
    branch_general(get_register(p.r, state), state);
})

IDEF_FN(BR, RI, {
    branch_general(get_register(p.r, state) + (((s32)(s16)p.i)) * 4, state);
})

IDEF_FN(BR, M, {
    branch_general(deref_get_memory(p.m, state), state);
})

IDEF_FN(BR, MI, {
    branch_general(get_memory(get_register(p.m, state) + (((s32)(s16)p.i) * 4), state), state);
})

IDEF_FN(CBR, I, {
    conditional_branch_general(*state->pc + (((s32) (s16) p.i) * 4), state);
})

IDEF_FN(CBR, R, {
    conditional_branch_general(get_register(p.r, state), state);
})

IDEF_FN(CBR, RI, {
    conditional_branch_general(get_register(p.r, state) + (s32)(p.i), state);
})

IDEF_FN(CBR, M, {
    conditional_branch_general(deref_get_memory(p.m, state), state);
})

IDEF_FN(CBR, MI, {
    conditional_branch_general(get_memory(get_register(p.m, state) + (((s32)(s16)p.i) * 4), state), state);
})

IDEF_FN(LDR, RI, {
    set_register(p.r, p.i, state);
})

IDEF_FN(LDR, MR, {
    set_register(p.r, deref_get_memory(p.m, state), state);
})

IDEF_FN(LDR, MRI, {
    set_register(p.r, get_memory(get_register(p.m, state) + ((s32)(s16)p.i), state), state);
})

IDEF_FN(STR, RR, {
    set_register(p.r2, get_register(p.r1, state), state);
})

IDEF_FN(STR, MR, {
    deref_set_memory(p.m, get_register(p.r, state), state);
})

IDEF_FN(STR, MRI, {
    set_memory(get_register(p.m, state) + ((s32)(s16)p.i), get_register(p.r, state), state);
})

IDEF_FN(OFF, RI, {
    offset_general(((s32)(s16)p.i), p.r, state);
})

IDEF_FN(OFF, RR, {
    offset_general((s32) get_register(p.r1, state), p.r2, state);
})

IDEF_FN(ABS, R, {
    set_register(p.r, abs((s32) get_register(p.r, state)), state);
})

IDEF_FN(CMP, RI, {
    compare_general(((s32)(s16)p.i), (s32)get_register(p.r, state), state);
})

IDEF_FN(CMP, RR, {
    compare_general((s32)get_register(p.r1, state), (s32)get_register(p.r2, state), state);
})

IDEF_FN(OR, RI, {
    or_general((s32) (s16) p.i, p.r, state);
})

IDEF_FN(OR, RR, {
    or_general((s32)get_register(p.r1, state), p.r2, state);
})

IDEF_FN(AND, RI, {
    and_general((s32) (s16) p.i, p.r, state);
})

IDEF_FN(AND, RR, {
    and_general((s32)get_register(p.r1, state), p.r2, state);
})

IDEF_FN(NOT, R, {
    set_register(p.r, ~get_register(p.r, state), state);
})

IDEF_FN(RO, RI, {
    ro_general(((s32)(s16)p.i), p.r, state);
})

IDEF_FN(RO, RR, {
    ro_general((s32)get_register(p.r1, state), p.r2, state);
})


IDEF_FN(RIN, I, {
    state->current_interrupt = be32toh(p.i) & 0xF;
})

IDEF_FN(SIM, I, {
    state->interrupt_mask = be32toh(p.i) & 0xFFFF;
})

IDEF_FN(HLT, N, {
    state->halted = true;
})

IDEF_FN(RST, N, {
    u8 *memory = state->memory;
    *state = CpuState{};
    (*state).memory = memory;
    state->branched = true;
})