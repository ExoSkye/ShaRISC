#include <antlr4-runtime.h>

#include <shariscasmLexer.h>
#include <shariscasmParser.h>
#include <shariscasmBaseListener.h>

#include <instructions/structs.hpp>
#include <instructions/enums.hpp>
#include <int_types.h>
#include <map>

#include <cstdarg>

std::map<std::string, u8> opcode_map {
        {"NOP", 0},
        {"BR", 1},
        {"CBR", 2},
        {"LDR", 3},
        {"STR", 4},
        {"OFF", 5},
        {"ABS", 6},
        {"CMP", 7},
        {"OR", 8},
        {"AND", 9},
        {"NOT", 10},
        {"RO", 11},
        {"RIN", 12},
        {"SIM", 13},
        {"HLT", 14},
        {"RST", 15},
};

u8 ensure_8bit(u64 in) {
    return *(u8*)&in;
}

u16 ensure_16bit(u64 in) {
    return *(u16*)&in;
}

u32 ensure_32bit(u64 in) {
    return *(u32*)&in;
}

s8 parse_s8(const std::string& val) {
    s64 i = strtol(val.c_str(), nullptr, 0);

    u8 out = ensure_8bit(i);

    return *(s8*)&out;
}

s16 parse_s16(const std::string& val) {
    s64 i = strtol(val.c_str(), nullptr, 0);

    u16 out = ensure_16bit(i);

    return *(s16*)&out;
}

s32 parse_s32(const std::string& val) {
    s64 i = strtol(val.c_str(), nullptr, 0);

    u32 out = ensure_32bit(i);

    return *(s32*)&out;
}

u8 parse_u8(const std::string& val) {
    u64 i = strtoul(val.c_str(), nullptr, 0);

    return ensure_8bit(i);
}

u16 parse_u16(const std::string& val) {
    u64 i = strtoul(val.c_str(), nullptr, 0);

    return ensure_16bit(i);
}

u32 parse_u32(const std::string& val) {
    u64 i = strtoul(val.c_str(), nullptr, 0);

    return ensure_32bit(i);
}

class LabelFinder : public Parser::shariscasmBaseListener {
    u32 cur_pc = 0;
    std::map<std::string, u32> labels;


    void enterLabel(Parser::shariscasmParser::LabelContext* ctx) override {
        labels[ctx->children[0]->getText()] = cur_pc;
    }

    void enterInstruction(Parser::shariscasmParser::InstructionContext *) override {
        cur_pc = (cur_pc + 3) & (~0b11);
        cur_pc += 4;
    }

    void enterDirective_align(Parser::shariscasmParser::Directive_alignContext* ctx) override {
        u32 align_val = parse_u32(ctx->children[1]->getText());
        cur_pc += (-cur_pc & (align_val - 1));
    }

    void enterDirective_at(Parser::shariscasmParser::Directive_atContext* ctx) override {
        cur_pc = parse_u32(ctx->children[0]->getText());
    }

    void enterDirective_skip(Parser::shariscasmParser::Directive_skipContext* ctx) override {
        cur_pc += parse_u32(ctx->children[0]->getText());
    }

    void enterDirective_byte(Parser::shariscasmParser::Directive_byteContext* ctx) override {
        cur_pc += 1;
    }

    void enterDirective_short(Parser::shariscasmParser::Directive_shortContext* ctx) override {
        cur_pc += 2;
    }

    void enterDirective_word(Parser::shariscasmParser::Directive_wordContext* ctx) override {
        cur_pc += 4;
    }

    void enterDirective_word_label(Parser::shariscasmParser::Directive_word_labelContext *ctx) override {
        cur_pc += 4;
    }

    void enterDirective_string(Parser::shariscasmParser::Directive_stringContext* ctx) override {
        std::string str = ctx->children[1]->getText().substr(1);
        str = str.substr(0, str.size() - 1);

        cur_pc += str.size();
    }

public:
    std::map<std::string, u32>& getLabels() {
        return labels;
    }
};

class CodeGenerator : public Parser::shariscasmBaseListener {
    u32 cur_pc = 0;
    std::unordered_map<u32, u8> output;
    std::map<std::string, u32> labels;
    instruction_t current_instruction{};
    u8 current_index = 0;
    meta_param_mode params{};

    static void what_the_fuck(const std::string& message) {
        printf("============ FATAL ============\n"
               "SOMETHING HAS MESSED UP\n"
               "YOU SHOULD NOT SEE THIS ERROR\n"
               "PLEASE REPORT ANY OCCURRENCE OF\n"
               "THIS ERROR TO ME, THE DEVELOPER\n"
               "AT `admin@spaceshark.gay`\n"
               "I, THE BEARER OF BAD NEWS HAVE\n"
               "BEEN ASKED TO PROVIDE YOU WITH\n"
               "THIS MESSAGE:\n"
               "`%s`\n"
               "THE PROGRAM WILL NOW EXIT\n", message.c_str());

        exit(-1);
    }

    void insert_u32(u32 idx, u32 val) {
        for (u8 i = 0; i < 4; i++) {
            output.insert({idx + i, (val >> i * 8) & 0xFF});
        }
    }

    void insert_u16(u32 idx, u16 val) {
        for (u8 i = 0; i < 2; i++) {
            output.insert({idx + i, (val >> i * 8) & 0xFF});
        }
    }

    void insert_u8(u32 idx, u8 val) {
        output.insert({idx, val});
    }

public:
    explicit CodeGenerator(std::map<std::string, u32>& _labels) {
        current_instruction = instruction_t{};
        current_index = 0;
        params = meta_param_mode{
            .all = 0
        };

        labels = std::move(_labels);
    }

    void enterInstruction(Parser::shariscasmParser::InstructionContext* ctx) override {
        current_instruction.opcode = opcode_map[ctx->children[0]->children[0]->getText()];
        cur_pc = (cur_pc + 3) & (~0b11); // Align pointer
    }

    void exitInstruction(Parser::shariscasmParser::InstructionContext* ctx) override {
        u32 be_inst = htobe32(*(u32 *) &current_instruction);

        insert_u32(cur_pc, be_inst);

        current_instruction = instruction_t{};
        params = meta_param_mode{
            .all = 0
        };
        current_index = 0;
        cur_pc += 4;
    }

#define DEF_PARAM_MODE(L, U) \
    void enterParam_mode_##L(Parser::shariscasmParser::Param_mode_##L##Context *) override { \
        current_instruction.param_mode = U; \
    }                        \
                             \
    void exitParam_mode_##L(Parser::shariscasmParser::Param_mode_##L##Context *) override { \
        current_instruction.parameters = params.all;                     \
    }

    DEF_PARAM_MODE(n, N)
    DEF_PARAM_MODE(i, I)
    DEF_PARAM_MODE(r, R)
    DEF_PARAM_MODE(rr, RR)
    DEF_PARAM_MODE(ri, RI)
    /* Currently unused
    DEF_PARAM_MODE(rii, RII)
    DEF_PARAM_MODE(rri, RRI)
    DEF_PARAM_MODE(rrr, RRR)
    DEF_PARAM_MODE(iii, III)
    */
    DEF_PARAM_MODE(m, M)
    DEF_PARAM_MODE(mr, MR)
    DEF_PARAM_MODE(mi, MI)
    DEF_PARAM_MODE(mri, MRI)

    s32 get_label_offset(const std::string& label, u8 multiplier = 1) {
        u32 label_address = get_label_address(label);

        if (std::abs((s64)label_address - (s64)cur_pc) > 0x7FFF * multiplier) {
            printf("CODE GENERATOR ERROR: Label is over %u bytes away, not possible to encode, exiting", 0x7FFF * multiplier);
            exit(-3);
        }

        return (s32)((s64)label_address - (s64)cur_pc);
    }

    u32 get_label_address(const std::string& label) {
        if (labels.find(label) == labels.end()) {
            printf("CODE GENERATOR ERROR: Label %s does not exist", label.c_str());
            exit(-2);
        }

        return labels[label];
    }

    void enterParam_mode_l(Parser::shariscasmParser::Param_mode_lContext* ctx) override {
        current_instruction.param_mode = I;

        std::string label_name = ctx->children[0]->children[0]->getText();

        s16 offset = (s16)(get_label_offset(label_name, 4) / 4);
        params.i.i = *(u16*)&offset;
    }

    void exitParam_mode_l(Parser::shariscasmParser::Param_mode_lContext *) override {
        current_instruction.parameters = params.all;
        current_index = 0;
    }

    void enterParam_mode_lr(Parser::shariscasmParser::Param_mode_lrContext* ctx) override {
        current_instruction.param_mode = MRI;

        std::string label_name = ctx->children[0]->children[0]->getText();

        s16 offset = get_label_offset(label_name);
        params.mri.i = *(u16*)&offset;
        params.mri.m = 15;
        current_index = 1;
    }

    void exitParam_mode_lr(Parser::shariscasmParser::Param_mode_lrContext *) override {
        current_instruction.parameters = params.all;
        current_index = 0;
    }

    void enterRegister(u8 value, Parser::shariscasmParser::RegisterContext* ctx) {
        switch (current_instruction.param_mode) {
            case R:
                if (current_index == 0) {
                    params.r.r = value;
                } else {
                    what_the_fuck("Incorrect parse tree (R, idx != 0)");
                }
                break;
            case RR:
                switch (current_index) {
                    case 0:
                        params.rr.r1 = value;
                        break;
                    case 1:
                        params.rr.r2 = value;
                        break;
                    default:
                        what_the_fuck("Incorrect parse tree (RR, idx < 0, idx > 1)");
                        break;
                }
                break;
            case RI:
                if (current_index == 0) {
                    params.ri.r = value;
                } else {
                    what_the_fuck("Incorrect parse tree (RI, idx != 0)");
                }
                break;
            case RII:
                if (current_index == 0) {
                    params.rii.r = value;
                } else {
                    what_the_fuck("Incorrect parse tree (RRI, idx != 0)");
                }
                break;
            case RRI:
                switch (current_index) {
                    case 0:
                        params.rri.r1 = value;
                        break;
                    case 1:
                        params.rri.r2 = value;
                        break;
                    default:
                        what_the_fuck("Incorrect parse tree (RRI, idx < 0, idx > 1)");
                        break;
                }
                break;
            case RRR:
                switch (current_index) {
                    case 0:
                        params.rrr.r1 = value;
                        break;
                    case 1:
                        params.rrr.r2 = value;
                        break;
                    case 2:
                        params.rrr.r3 = value;
                        break;
                    default:
                        what_the_fuck("Incorrect parse tree (RRR, idx < 0, idx > 2)");
                        break;
                }
                break;
            case M:
                if (current_index == 0) {
                    params.m.m = value;
                } else {
                    what_the_fuck("Incorrect parse tree (M, idx != 0)");
                }
                break;
            case MI:
                if (current_index == 0) {
                    params.mi.m = value;
                } else {
                    what_the_fuck("Incorrect parse tree (MI, idx != 0)");
                }
                break;
            case MR:
                switch (current_index) {
                    case 0:
                        params.mr.m = value;
                        break;
                    case 1:
                        params.mr.r = value;
                        break;
                    default:
                        what_the_fuck("Incorrect parse tree (MR, idx < 0, idx > 1)");
                        break;
                }
                break;
            case MRI:
                switch (current_index) {
                    case 0:
                        params.mri.m = value;
                        break;
                    case 1:
                        params.mri.r = value;
                        break;
                    default:
                        what_the_fuck("Incorrect parse tree (MRI, idx < 0, idx > 1)");
                        break;
                }
                break;
            default:
                what_the_fuck("Incorrect parse tree (Parameter Mode not found in Registers)");
                break;
        }

        if (current_instruction.param_mode != 0) {
            current_index++;
        }
    }

    void enterInt(Parser::shariscasmParser::IntContext* ctx) override {
        switch (current_instruction.param_mode) {
            case I:
                if (current_index == 0) {
                    params.i.i = parse_s16(ctx->getText());
                } else {
                    what_the_fuck("Incorrect parse tree (I, idx != 0)");
                }
                break;
            case RI:
                if (current_index == 1) {
                    params.ri.i = parse_s16(ctx->getText());
                } else {
                    what_the_fuck("Incorrect parse tree (RI, idx != 1)");
                }
                break;
            case RII:
                switch (current_index) {
                    case 1:
                        params.rii.i1 = parse_s8(ctx->getText());
                        break;
                    case 2:
                        params.rii.i2 = parse_s8(ctx->getText());
                        break;
                    default:
                        what_the_fuck("Incorrect parse tree (RII, idx < 1, idx > 2)");
                        break;
                }
                break;
            case RRI:
                if (current_index == 2) {
                    params.rri.i = parse_s16(ctx->getText());
                } else {
                    what_the_fuck("Incorrect parse tree (RRI, idx != 2)");
                }
                break;
            case III:
                switch (current_index) {
                    case 0:
                        params.iii.i1 = parse_s8(ctx->getText());
                        break;
                    case 1:
                        params.iii.i2 = parse_s8(ctx->getText());
                        break;
                    case 2:
                        params.iii.i3 = parse_s8(ctx->getText());
                        break;
                    default:
                        what_the_fuck("Incorrect parse tree (III, idx < 0, idx > 2)");
                        break;
                }
                break;
            case MI:
                if (current_index == 1) {
                    params.mi.i = parse_s16(ctx->getText());
                } else {
                    what_the_fuck("Incorrect parse tree (MI, idx != 1)");
                }
                break;
            case MRI:
                if (current_index == 2) {
                    params.mri.i = parse_s16(ctx->getText());
                } else {
                    what_the_fuck("Incorrect parse tree (MRI, idx != 2)");
                }
                break;
            case 0:
                // Skip because this isn't an instruction
                break;
            default:
                what_the_fuck("Incorrect parse tree (Parameter Mode not found in Immediate)");
                break;

        }

        if (current_instruction.param_mode != 0) {
            current_index++;
        }
    }

#define DEF_REGISTER_ENTRY(VALUE) \
    void enterR##VALUE(Parser::shariscasmParser::R##VALUE##Context* ctx) override { \
        enterRegister(VALUE, (Parser::shariscasmParser::RegisterContext*)ctx->parent); \
    }

    DEF_REGISTER_ENTRY(0)
    DEF_REGISTER_ENTRY(1)
    DEF_REGISTER_ENTRY(2)
    DEF_REGISTER_ENTRY(3)
    DEF_REGISTER_ENTRY(4)
    DEF_REGISTER_ENTRY(5)
    DEF_REGISTER_ENTRY(6)
    DEF_REGISTER_ENTRY(7)
    DEF_REGISTER_ENTRY(8)
    DEF_REGISTER_ENTRY(9)
    DEF_REGISTER_ENTRY(10)
    DEF_REGISTER_ENTRY(11)
    DEF_REGISTER_ENTRY(12)
    DEF_REGISTER_ENTRY(13)
    DEF_REGISTER_ENTRY(14)
    DEF_REGISTER_ENTRY(15)

    void enterDirective_at(Parser::shariscasmParser::Directive_atContext* ctx) override {
        cur_pc = parse_u32(ctx->children[1]->getText());
    }

    void enterDirective_skip(Parser::shariscasmParser::Directive_skipContext* ctx) override {
        cur_pc += parse_u32(ctx->children[1]->getText());
    }

    void enterDirective_align(Parser::shariscasmParser::Directive_alignContext* ctx) override {
        u32 align_val = parse_u32(ctx->children[1]->getText());
        cur_pc += (-cur_pc & (align_val - 1));
    }

    void enterDirective_byte(Parser::shariscasmParser::Directive_byteContext* ctx) override {
        insert_u8(cur_pc, parse_u8(ctx->children[1]->getText()));
        cur_pc += 1;
    }

    void enterDirective_short(Parser::shariscasmParser::Directive_shortContext* ctx) override {
        insert_u16(cur_pc, parse_u16(ctx->children[1]->getText()));
        cur_pc += 2;
    }

    void enterDirective_word(Parser::shariscasmParser::Directive_wordContext* ctx) override {
        insert_u32(cur_pc, parse_u32(ctx->children[1]->getText()));
        cur_pc += 4;
    }

    void enterDirective_word_label(Parser::shariscasmParser::Directive_word_labelContext *ctx) override {
        insert_u32(cur_pc, get_label_address(ctx->children[2]->getText()));
        cur_pc += 4;
    }

    void enterDirective_string(Parser::shariscasmParser::Directive_stringContext* ctx) override {
        std::string str = ctx->children[1]->getText().substr(1);
        str = str.substr(0, str.size() - 1);

        for (auto chr : str) {
            insert_u8(cur_pc, chr);
            cur_pc++;
        }
    }

    std::unordered_map<u32, u8>& getOutput() {
        return output;
    }
};

int main(int argc, char** argv) {
    if (argc != 3) {
        printf("Usage: %s [source file] [output file]\n", argv[0]);
        exit(-1);
    }

    antlr4::ANTLRFileStream stream;
    stream.loadFromFile(argv[1]);

    Parser::shariscasmLexer lexer(&stream);
    antlr4::CommonTokenStream tokens(&lexer);
    tokens.fill();
    Parser::shariscasmParser parser(&tokens);

    antlr4::tree::ParseTree* tree = parser.instruction_list();

    LabelFinder labelFinder;

    antlr4::tree::ParseTreeWalker walker;
    walker.walk(&labelFinder, tree);


    CodeGenerator codeGenerator(labelFinder.getLabels());
    walker.walk(&codeGenerator, tree);

    FILE* f = fopen(argv[2], "w+");

    std::unordered_map<u32, u8>& output = codeGenerator.getOutput();

    for (auto pair : output) {
        fseek(f, pair.first, SEEK_SET);
        fwrite(&pair.second, 1, 1, f);
    }

    fclose(f);
}